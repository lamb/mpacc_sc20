import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

data = pd.read_csv('data/perf_var.csv')
max_height = 750

sns.set_style("darkgrid", {"font.family": ['serif'], "font.serif": ['Computer Modern']})
sns.set_palette("cubehelix", 5)

g = sns.catplot(data=data, kind="bar", x="version", y="seconds", 
                  col="device", row="language", legend=False)

g.set_axis_labels("", "Runtime(s)")
g.set(ylim=(0, max_height))
g.set_titles("{row_name} {col_name}")

axes=g.axes #annotate axis = seaborn axis

for i in [0, 1]:
  for j in [0, 1]:
    ax = axes[i][j]
    for p in ax.patches:
      if p.get_height() > max_height:
        ax.annotate("%.2f" % p.get_height(), 
                 (p.get_x() + p.get_width() / 2., max_height - 175),
                 ha='center', va='top', fontsize=9, color='black', rotation=90, 
                 xytext=(0, 20), textcoords='offset points')  

plt.show()
