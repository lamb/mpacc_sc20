import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

data = pd.read_csv('data/pgi_omp.csv')
data = data[(data != 0).all(1)]

max_height = 500

sns.set_style("darkgrid", {"font.family": ['serif'], "font.serif": ['Computer Modern']})
#sns.set_palette("cubehelix", 5)

g = sns.catplot(data=data, kind="bar", x="benchmark", y="seconds", 
                  hue="source", col="device", legend=False)

g.set_axis_labels("", "Runtime(s)")
g.set(ylim=(0, max_height))
g.set_titles("{col_name}")

axes=g.axes #annotate axis = seaborn axis
for i in [0]:
  for j in range(0,2):
    ax = axes[i][j]
    for p in ax.patches:
      if p.get_height() > max_height:
        ax.annotate("%.2f" % p.get_height(), 
                 (p.get_x() + p.get_width() / 2., max_height * 0.8),
                 ha='center', va='top', fontsize=9, color='black', rotation=90, 
                 xytext=(0, 20), textcoords='offset points')  

plt.show()
