
# --------  Configuration -------- 

# Select whcih versions, targets, and compilers to build/execute (set as 0 or 1)
RUN_MAN_ACC=1   # Baseline
RUN_CCAMP_OMP=1 # OpenACC to OpenMP Translation 
RUN_CCAMP_ACC_OPT=1 # OpenACC Optimization
RUN_CCAMP_OMP_OPT=1 # OpenACC to OpenMP Translation + OpenMP Optimization

RUN_XEON=1
RUN_P100=1
# Test scripts for POW9 and V100 evaluation are not included.
#RUN_POW9=0
#RUN_V100=0

RUN_CLG=1
RUN_PGI=1
RUN_GCC=1
# Test scripts for the IBM compiler are not included.
#RUN_IBM=0

# Set names of modules needed to be loaded for respective compilers
# This script assumes that both PGI and Clang compilers can be loaded using module commands.
# If not, set USE_PGI/CLANG_MOD to 0 and manually set up the necessary environment for each compiler for each test.
USE_CLANG_MOD=1
CLANG_MOD="gnu/7.5.0"
#PGI_MOD="pgi/pgi/19.4"
#PGI_V=19.4
USE_PGI_MOD=0
PGI_MOD="pgi/pgi/19.10"
PGI_V=19.10

# Set spack-related setting for GCC compiler.
SPACK_ROOT="${HOME}/spack"
SPACK_SETUP_FILE="${SPACK_ROOT}/share/spack/setup-env.sh"

SPACK_CUDA=1

if [ ${RUN_GCC} -eq 1 ]; then
	if [ ! -f ${SPACK_SETUP_FILE} ]; then
		echo "Cannot find the spack setup file (${SPACK_SETUP_FILE}), which is needed for GCC tests; please set up correct spack-root path (SPACK_ROOT) or disable GCC tests by setting RUN_GCC to 0 in this script; exit!"
		exit
	fi
fi

# Set Log file for build commands, lulesh execution output
LULESH_LOG=lulesh_log.txt
rm -f ${LULESH_LOG}

# Lulesh raw times file
LULESH_TIMES=lulesh_times.txt
rm -f ${LULESH_TIMES}

# Set number of iterations
ITER=1477       # Default
ITER_SHORT=14   # Used for configurations where the full execution time is too high for reasonable analysis

# --------  Execution -------- 

#LULESH_V=0
#if [[ LULESH_V == 0 ]]
#then
# exec 1> >(tee ${LULESH_LOG})  2>&1
#fi

export TIMEFORMAT=%R;
# Xeon CPU
#  RUN_ACC
#    pgi
#  RUN_CCAMP_ACC_OPT
#    pgi
#  RUN_CCAMP_OMP
#    pgi
#    clang
#  RUN_CCAMP_OMP_OPT
#    pgi
#    clang
if [[ $RUN_XEON == 1 ]]
then

  if [[ $RUN_MAN_ACC == 1 ]]
  then

    if [[ $RUN_PGI == 1 ]]
    then
      echo "-------------- MAN_ACC, xeon, pgi -----------";
      if [ ${USE_PGI_MOD} -eq 1 ]; then
        module purge 
        module load ${PGI_MOD}
      else
        which pgcc > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the pgcc compiler; please sep up the script correctly for MAN_ACC, xeon, pgi test"
          echo ""
        fi
      fi
      make clean 
      which pgcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="pgcc" LULESH_CFLAGS=" -I. -Minfo=mp,acc -V${PGI_V} -Mllvm -fast -acc -mp -Mnouniform -ta=multicore" LULESH_CSRCS="*.c" 

        ( time ./lulesh -s 45 -p -i ${ITER_SHORT} ) 2>time_tmp
        RUNTIME=$(echo "$(cat time_tmp) * 100" | bc) # extrapolate
        rm time_tmp

        echo "MAN_ACC, xeon, pgi, ${RUNTIME}" 
        echo "MAN_ACC, xeon, pgi, ${RUNTIME}" >> ${LULESH_TIMES} 
      else
        echo ""
        echo "==> [ERROR] Cannot find the pgcc compiler; skip the tests!"
        echo ""
      fi
    fi
  fi

  if [[ $RUN_CCAMP_ACC_OPT == 1 ]]
  then
    echo "------------  Xeon CCAMP_ACC_OPT Build ------------------" 
    make purge
    openarc -verbosity=0 -gpuConfFile=openarcArgs.txt -parallelismMappingStrat=enableAdvancedMapping=1:vectorize=0:vectorfriendlyanalysis=2:devicetype=1 -SkipGPUTranslation=1 *.c 

    if [[ $RUN_PGI == 1 ]]
    then

      echo "-------------- CCAMP_ACC_OPT, xeon, pgi -----------"  
      if [ ${USE_PGI_MOD} -eq 1 ]; then
        module purge 
        module load ${PGI_MOD} 
      else
        which pgcc > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the pgcc compiler; please sep up the script correctly for CCAMP_ACC_OPT, xeon, pgi test"
          echo ""
        fi
      fi
      make clean
      which pgcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
  
        # Bug in PGI compiler causes error when targing CPU with firstprivate() clause
        sed -i 's/firstprivate.*//' cetus_output/lulesh.c

        make  LULESH_CXX="pgcc" LULESH_CFLAGS=" -I. -Minfo=mp,acc -V${PGI_V} -Mllvm -fast -acc -mp -Mnouniform -ta=multicore" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_ACC_OPT, xeon, pgi, ${RUNTIME}" 
        echo "CCAMP_ACC_OPT, xeon, pgi, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the pgcc compiler; skip the tests!"
        echo ""
      fi
    fi
  fi

  if [[ $RUN_CCAMP_OMP == 1 ]]
  then
    echo "------------  Xeon CCAMP_OMP Build ------------------" 
    make purge 
    openarc -verbosity=0 -gpuConfFile=openarcArgs.txt -ompaccInter=5 -SkipGPUTranslation=1 *.c 

    if [[ $RUN_CLG == 1 ]]
    then

      echo "-------------- CCAMP_OMP, xeon, clang -------------"  
      if [ ${USE_CLANG_MOD} -eq 1 ]; then
        module purge 
        module load ${CLANG_MOD}
      else
        which clang > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the clang compiler; please sep up the script correctly for CCAMP_OMP, xeon, clang test"
          echo ""
        fi
      fi
      make clean 
      which clang > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="clang" LULESH_CFLAGS="-Wall -O3 -I. -fopenmp -fopenmp-targets=x86_64-unknown-linux-gnu -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_OMP, xeon, clang, ${RUNTIME}"
        echo "CCAMP_OMP, xeon, clang, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the clang compiler; skip the tests!"
        echo ""
      fi
    fi

    if [[ $RUN_PGI == 1 ]]
    then

      echo "-------------- CCAMP_OMP, xeon, pgi -------------" 
      if [ ${USE_PGI_MOD} -eq 1 ]; then
        module purge 
        module load ${PGI_MOD}
      else
        which pgcc > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the pgcc compiler; please sep up the script correctly for CCAMP_OMP, xeon, pgi test"
          echo ""
        fi
      fi
      make clean  
      which pgcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="pgcc" LULESH_CFLAGS="-I. -Minfo=mp,acc -V${PGI_V} -Mllvm -fast -acc -mp -Mnouniform -ta=multicore" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_OMP, xeon, pgi, ${RUNTIME}" 
        echo "CCAMP_OMP, xeon, pgi, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the pgcc compiler; skip the tests!"
        echo ""
      fi
    fi

  fi

  if [[ $RUN_CCAMP_OMP_OPT == 1 ]]
  then
    echo "------------  Xeon CCAMP_OMP_OPT Build ------------------" 
    make purge 
    openarc -verbosity=0 -gpuConfFile=openarcArgs.txt -ompaccInter=5 -parallelismMappingStrat=enableAdvancedMapping=1:vectorize=0:vectorfriendlyanalysis=2:devicetype=1 -SkipGPUTranslation=1 *.c 

    if [[ $RUN_CLG == 1 ]]
    then

      echo "-------------- CCAMP_OMP_OPT, xeon, clang -------------"  
      if [ ${USE_CLANG_MOD} -eq 1 ]; then
        module purge 
        module load ${CLANG_MOD}
      else
        which clang > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the clang compiler; please sep up the script correctly for CCAMP_OMP_OPT, xeon, clang test"
          echo ""
        fi
      fi
      make clean 
      which clang > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="clang" LULESH_CFLAGS="-Wall -O3 -I. -fopenmp -fopenmp-targets=x86_64-unknown-linux-gnu -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_OMP_OPT, xeon, clang, ${RUNTIME}"
        echo "CCAMP_OMP_OPT, xeon, clang, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the clang compiler; skip the tests!"
        echo ""
      fi
    fi

    if [[ $RUN_PGI == 1 ]]
    then

      echo "-------------- CCAMP_OMP_OPT, xeon, pgi -------------" 
      if [ ${USE_PGI_MOD} -eq 1 ]; then
        module purge 
        module load ${PGI_MOD}
      else
        which pgcc > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the pgcc compiler; please sep up the script correctly for CCAMP_OMP_OPT, xeon, pgi test"
          echo ""
        fi
      fi
      make clean  
      which pgcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="pgcc" LULESH_CFLAGS="-I. -Minfo=mp,acc -V${PGI_V} -Mllvm -fast -acc -mp -Mnouniform -ta=multicore" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_OMP_OPT, xeon, pgi, ${RUNTIME}" 
        echo "CCAMP_OMP_OPT, xeon, pgi, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the pgcc compiler; skip the tests!"
        echo ""
      fi
    fi

  fi
fi


# P100 
#  RUN_MAN_ACC
#    pgi 
#    gcc
#  RUN_CCAMP_ACC_OPT
#    pgi
#    gcc
#  RUN_CCAMP_OMP
#    clang
#    gcc 
#  RUN_CCAMP_OMP_OPT
#    clang
#    gcc 
if [[ $RUN_P100 == 1 ]]
then

  if [[ $RUN_MAN_ACC == 1 ]]
  then
    if [[ $RUN_PGI == 1 ]]
      then
      echo "-------------- MAN_ACC, p100, pgi -----------";
      if [ ${USE_PGI_MOD} -eq 1 ]; then
        module purge 
        module load ${PGI_MOD}
      else
        which pgcc > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the pgcc compiler; please sep up the script correctly for MAN_ACC, p100, pgi test"
          echo ""
        fi
      fi
      make clean 
      which pgcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="pgcc" LULESH_CFLAGS=" -I. -Minfo=mp,acc -V${PGI_V} -Mllvm -fast -acc -mp -Mnouniform -ta=tesla:cc60" LULESH_CSRCS="*.c" 

        ( time ./lulesh -s 45 -p -i ${ITER_SHORT} ) 2>time_tmp
        RUNTIME=$(echo "$(cat time_tmp) * 100" | bc) # extrapolate
        rm time_tmp

        echo "MAN_ACC, p100, pgi, ${RUNTIME}" 
        echo "MAN_ACC, p100, pgi, ${RUNTIME}" >> ${LULESH_TIMES} 
      else
        echo ""
        echo "==> [ERROR] Cannot find the pgcc compiler; skip the tests!"
        echo ""
      fi
    fi

    if [[ $RUN_GCC == 1 ]]
    then

      echo "-------------- MAN_ACC, p100, gcc -----------"  
      if [[ ${USE_CLANG_MOD} == 1 || ${USE_PGI_MOD} == 1 ]]; then
        module purge 
      fi
      source ${SPACK_SETUP_FILE} 
      spack load gcc
      make clean
      which gcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="gcc" LULESH_CFLAGS="-I. -Wall -O3 -fopenacc -foffload="-lm" -lm" LULESH_CSRCS="*.c"

        ( time ./lulesh -s 45 -p -i ${ITER_SHORT} ) 2>time_tmp
        #RUNTIME=$(cat time_tmp)
        RUNTIME=$(echo "$(cat time_tmp) * 100" | bc) # extrapolate
        rm time_tmp

        echo "MAN_ACC, p100, gcc, ${RUNTIME}" 
        echo "MAN_ACC, p100, gcc, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the gcc compiler; skip the tests!"
        echo ""
      fi

      spack unload --all 

    fi

   
  fi

  if [[ $RUN_CCAMP_ACC_OPT == 1 ]]
  then
    echo "------------  p100 CCAMP_ACC_OPT Build ------------------" 
    make purge
    openarc -verbosity=0 -gpuConfFile=openarcArgs.txt -parallelismMappingStrat=enableAdvancedMapping=1:vectorize=0:vectorfriendlyanalysis=2:devicetype=0 -SkipGPUTranslation=1 *.c 

    if [[ $RUN_PGI == 1 ]]
    then

      echo "-------------- CCAMP_ACC_OPT, p100, pgi -----------"  
      if [ ${USE_PGI_MOD} -eq 1 ]; then
        module purge 
        module load ${PGI_MOD} 
      else
        which pgcc > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the pgcc compiler; please sep up the script correctly for CCAMP_ACC_OPT, p100, pgi test"
          echo ""
        fi
      fi

      if [ ${SPACK_CUDA} -eq 1 ]; then
        source ${SPACK_SETUP_FILE}
        spack load cuda
      fi
    
      make clean
      which pgcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then

        # address PGI bugs and temporary MPACC workaround
        sed -i 's/firstprivate.*//' cetus_output/lulesh.c
        sed -i 's/num_workers(64)//' cetus_output/lulesh.c
        sed -i 's/gang(((int)ceil((((float)numNode)\/64.0F))))//' cetus_output/lulesh.c
        sed -i 's/gang(((int)ceil((((float)numElem)\/64.0F))))//' cetus_output/lulesh.c
        sed -i 's/gang//' cetus_output/lulesh.c
        sed -i 's/worker(64)//' cetus_output/lulesh.c
        sed -i 's/worker//' cetus_output/lulesh.c
        sed -i 's/vector(256)//' cetus_output/lulesh.c
        sed -i 's/vector//' cetus_output/lulesh.c

        make  LULESH_CXX="pgcc" LULESH_CFLAGS=" -I. -Minfo=mp,acc -V${PGI_V} -Mllvm -fast -acc -mp -Mnouniform -ta=tesla:cc60" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_ACC_OPT, p100, pgi, ${RUNTIME}" 
        echo "CCAMP_ACC_OPT, p100, pgi, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the pgcc compiler; skip the tests!"
        echo ""
      fi

      if [ ${SPACK_CUDA} -eq 1 ]; then
        spack unload --all
      fi

    fi

    if [[ $RUN_GCC == 1 ]]
    then

      echo "-------------- CCAMP_ACC_OPT, p100, gcc -----------"  
      if [[ ${USE_CLANG_MOD} == 1 || ${USE_PGI_MOD} == 1 ]]; then
        module purge 
      fi
      source ${SPACK_SETUP_FILE} 
      spack load gcc
      make clean
      which gcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="gcc" LULESH_CFLAGS="-I. -Wall -O3 -fopenacc -foffload="-lm" -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER_SHORT} ) 2>time_tmp
        #RUNTIME=$(cat time_tmp)
        RUNTIME=$(echo "$(cat time_tmp) * 100" | bc) # extrapolate
        rm time_tmp

        echo "CCAMP_ACC_OPT, p100, gcc, ${RUNTIME}" 
        echo "CCAMP_ACC_OPT, p100, gcc, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the gcc compiler; skip the tests!"
        echo ""
      fi

      spack unload --all 

    fi
  fi

  if [[ $RUN_CCAMP_OMP == 1 ]]
  then
    echo "------------  p100 CCAMP_OMP Build ------------------" 
    make purge 
    openarc -verbosity=0 -gpuConfFile=openarcArgs.txt -ompaccInter=5 -SkipGPUTranslation=1 *.c 

    if [[ $RUN_CLG == 1 ]]
    then

      echo "-------------- CCAMP_OMP, p100, clang -------------"  
      if [ ${USE_CLANG_MOD} -eq 1 ]; then
        module purge 
        module load ${CLANG_MOD}
      else
        which clang > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the clang compiler; please sep up the script correctly for CCAMP_OMP, p100, clang test"
          echo ""
        fi
      fi
      make clean 
      which clang > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="clang" LULESH_CFLAGS="-Wall -O3 -I. -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda --cuda-gpu-arch=sm_60 -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_OMP, p100, clang, ${RUNTIME}"
        echo "CCAMP_OMP, p100, clang, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the clang compiler; skip the tests!"
        echo ""
      fi
    fi


    if [[ $RUN_GCC == 1 ]]
    then

      echo "-------------- CCAMP_OMP, p100, gcc -----------"  

      if [[ ${USE_CLANG_MOD} == 1 || ${USE_PGI_MOD} == 1 ]]; then
        module purge 
      fi
      source ${SPACK_SETUP_FILE} 
      spack load gcc
      make clean
      which gcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="gcc" LULESH_CFLAGS="-I. -Wall -O3 -fopenmp -foffload="-lm" -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER_SHORT} ) 2>time_tmp
        #RUNTIME=$(cat time_tmp)
        RUNTIME=$(echo "$(cat time_tmp) * 100" | bc) # extrapolate
        rm time_tmp

        echo "CCAMP_OMP, p100, gcc, ${RUNTIME}" 
        echo "CCAMP_OMP, p100, gcc, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the gcc compiler; skip the tests!"
        echo ""
      fi

      spack unload --all 

    fi

  fi


  if [[ $RUN_CCAMP_OMP_OPT == 1 ]]
  then
    echo "------------  p100 CCAMP_OMP_OPT Build ------------------" 
    make purge 
    openarc -verbosity=0 -gpuConfFile=openarcArgs.txt -ompaccInter=5 -parallelismMappingStrat=enableAdvancedMapping=1:vectorize=0:vectorfriendlyanalysis=2:devicetype=0 -SkipGPUTranslation=1 *.c 

    if [[ $RUN_CLG == 1 ]]
    then

      echo "-------------- CCAMP_OMP_OPT, p100, clang -------------"  
      if [ ${USE_CLANG_MOD} -eq 1 ]; then
        module purge 
        module load ${CLANG_MOD}
      else
        which clang > /dev/null 2>&1
        if [ $? -ne 0 ]
        then
          echo ""
          echo "==> [ERROR] Cannot find the clang compiler; please sep up the script correctly for CCAMP_OMP_OPT, p100, clang test"
          echo ""
        fi
      fi
      make clean 
      which clang > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="clang" LULESH_CFLAGS="-Wall -O3 -I. -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda --cuda-gpu-arch=sm_60 -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER} ) 2>time_tmp
        RUNTIME=$(cat time_tmp)
        rm time_tmp

        echo "CCAMP_OMP_OPT, p100, clang, ${RUNTIME}"
        echo "CCAMP_OMP_OPT, p100, clang, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the clang compiler; skip the tests!"
        echo ""
      fi
    fi


    if [[ $RUN_GCC == 1 ]]
    then

      echo "-------------- CCAMP_OMP_OPT, p100, gcc -----------"  

      # re-run OpenARC to change device type
      if [[ ${USE_CLANG_MOD} == 1 || ${USE_PGI_MOD} == 1 ]]; then
        module purge 
      fi
      source ${SPACK_SETUP_FILE} 
      spack load gcc
      make clean
      which gcc > /dev/null 2>&1
      if [ $? -eq 0 ]
      then
        make  LULESH_CXX="gcc" LULESH_CFLAGS="-I. -Wall -O3 -fopenmp -foffload="-lm" -lm" LULESH_CSRCS="cetus_output/*.c"

        ( time ./lulesh -s 45 -p -i ${ITER_SHORT} ) 2>time_tmp
        #RUNTIME=$(cat time_tmp)
        RUNTIME=$(echo "$(cat time_tmp) * 100" | bc) # extrapolate
        rm time_tmp

        echo "CCAMP_OMP_OPT, p100, gcc, ${RUNTIME}" 
        echo "CCAMP_OMP_OPT, p100, gcc, ${RUNTIME}" >> ${LULESH_TIMES}
      else
        echo ""
        echo "==> [ERROR] Cannot find the gcc compiler; skip the tests!"
        echo ""
      fi

      spack unload --all

    fi

  fi
fi
