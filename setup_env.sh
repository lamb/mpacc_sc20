# This sets up the environment path variables to point to the install prefix
INSTALL_PREFIX="$(pwd)/install"
echo "Initializing prefix: $INSTALL_PREFIX"
export PATH="$INSTALL_PREFIX/bin:$PATH"
if [ -n "$CPATH" ]; then
   export CPATH=$CPATH:$INSTALL_PREFIX/include
else
   export CPATH=$INSTALL_PREFIX/include
fi

echo "==> Install OpenARC files in $INSTALL_PREFIX directory."
mkdir -p $INSTALL_PREFIX/include
mkdir -p $INSTALL_PREFIX/bin
mkdir -p $INSTALL_PREFIX/lib
echo "#! /bin/sh" > $INSTALL_PREFIX/bin/openarc
echo "java -cp $INSTALL_PREFIX/lib/antlr.jar:$INSTALL_PREFIX/lib/cetus.jar -Xmx1g openacc.exec.ACC2GPUDriver \$*" >> $INSTALL_PREFIX/bin/openarc
chmod 755 $INSTALL_PREFIX/bin/openarc
cp -f ./openarc/lib/antlr.jar $INSTALL_PREFIX/lib/
cp -f ./openarc/lib/cetus.jar $INSTALL_PREFIX/lib/
cp -f ./openarc/openarcrt/openacc.h $INSTALL_PREFIX/include/
cp -f ./openarc/openarcrt/openaccrt.h $INSTALL_PREFIX/include/
cp -f ./openarc/openarcrt/resilience.h $INSTALL_PREFIX/include/
cp -f ./openarc/openarcrt/omp4_device.h $INSTALL_PREFIX/include/
cp -f ./openarc/openarcrt/omp_helper.h $INSTALL_PREFIX/include/
cp -f ./openarc/openarcrt/mcl_accext.h $INSTALL_PREFIX/include/
cp -f ./openarc/openarcrt/profile.h $INSTALL_PREFIX/include/


