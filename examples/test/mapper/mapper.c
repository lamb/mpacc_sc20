#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

      //extern int __pgi_gangidx(void);
      //extern int __pgi_workeridx(void);
      //extern int __pgi_vectoridx(void);

int main() {

  int a[1000], b[1000], c[1000];

  for (int i = 0; i < 1000; ++i) {
    a[i] = i;
    b[i] = 2*i;
  }

#if 1
  #pragma omp target map(to:a[0:1000], b[0:1000]) map(from:c[0:1000])
  {

    #pragma omp teams distribute
    for (int i = 0; i < 2000000; ++i) {
#pragma omp parallel for
      for (int j = 0; j < 13990; ++j) {
#pragma omp simd
        for (int k = 0; k < 2342; ++k) {
          if (i == 0 && j == 0 && k == 0) {
          printf("num_teams: %d\n", omp_get_num_teams());
          printf("num_threads: %d\n", omp_get_num_threads());
          }

          int j=0, k=0;
          int index = (i + j + k) % 1000;
          c[index] = a[index] + b[index]; 


        }
      }
    }

  }
#endif

#if 0
  #pragma acc parallel copyin(a[0:1000], b[0:1000]) copyout(c[0:1000]) 
  {

#pragma acc loop gang 
    for (int i = 0; i < 20; ++i) {
#pragma acc loop worker
      for (int j = 0; j < 143; ++j) {
#pragma acc loop vector
      for (int k = 0; k < 3; ++k) {
      //if (i == 0) {
        //printf("gang: %d\n", __pgi_gangidx());
        printf("worker: %d\n", __pgi_workeridx());
        //printf("vector: %d\n", __pgi_vectoridx());
      //}
      //int j=0, k=0;
      //int index = (i + j + k) % 1000;
      //c[index] = a[index] + b[index]; 

      }
      }
    }

  }
#endif

  printf("Verification Succesful!\n");

  return 0;
}

