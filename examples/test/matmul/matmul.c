#include "defines.h"
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <math.h>

double my_timer ()
{
  struct timeval time;

  gettimeofday (&time, 0); 

  return time.tv_sec + time.tv_usec / 1000000.0;
}

void MatrixMultiplication_directive(float * a, float * b, float * c)
{
  int i, j, k ;



// -----------------------------------
// OpenMP 4.X+ 

#if VERSION_OM4 != 0
#pragma omp target data map(to: b[0:(M*P)], c[0:(P*N)]) map(from: a[0:(M*N)])
#endif

// OpenMP Naive
#if VERSION_OM4 == 1
  #pragma omp target teams distribute
  for (i=0; i<M; i++){
    #pragma omp parallel for
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// OpenMP CPU (simd 1) 
#if VERSION_OM4 == 2
  #pragma omp target teams distribute parallel for 
  for (i=0; i<M; i++){
    #pragma omp simd
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// OpenMP CPU (simd 2) 
#if VERSION_OM4 == 3
  #pragma omp target teams distribute parallel for collapse(2)
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      #pragma omp simd
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif
  
// OpenMP CPU (no simd) 
#if VERSION_OM4 == 4
  #pragma omp target teams distribute parallel for collapse(2)
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      #pragma omp simd
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif
  
// OpenMP GPU 
#if VERSION_OM4 == 5
  #pragma omp target teams distribute parallel for collapse(2)
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// Old Best
#if VERSION_OM4 == 6
  #pragma omp target teams distribute parallel for 
  for (i=0; i<M; i++){
    #pragma omp simd
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif


// -----------------------------------
// OpenACC 2.0

// OpenACC Naive 
#if VERSION_ACC == 1
  #pragma acc parallel loop copyout(a[0:(M*N)]), copyin(b[0:(M*P)],c[0:(P*N)])
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// OpenACC (no simd) 
#if VERSION_ACC == 2
  #pragma acc parallel loop copyout(a[0:(M*N)]), copyin(b[0:(M*P)],c[0:(P*N)])  collapse(2)
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      #pragma acc loop seq
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// OpenACC (simd 1) 
#if VERSION_ACC == 3
  #pragma acc parallel loop copyout(a[0:(M*N)]), copyin(b[0:(M*P)],c[0:(P*N)]) collapse(2)
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      #pragma acc loop vector
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// OpenACC (simd 2) 
#if VERSION_ACC == 4
  #pragma acc parallel loop gang copyout(a[0:(M*N)]), copyin(b[0:(M*P)],c[0:(P*N)]) 
  for (i=0; i<M; i++){
    #pragma acc loop vector
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      #pragma acc loop
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif

// Old Best
#if VERSION_ACC == 5
  #pragma acc parallel loop gang worker num_gangs(M) num_workers(N) \
    copyout(a[0:(M*N)]), copyin(b[0:(M*P)],c[0:(P*N)]) \
    collapse(2)
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0F;
      #pragma acc loop seq
      for (k=0; k<P; k++) {
        sum += b[i*P+k]*c[k*N+j] ;
      }
      a[i*N+j] = sum ;
    }
  }
#endif


}


void MatrixMultiplication_verify(float * a,float * b, float * c)
{
  int i, j, k ;
  for (i=0; i<M; i++){
    for (j=0; j<N; j++)
    {
      float sum = 0.0 ;
      for (k=0; k<P; k++)
        sum += b[i*P+k]*c[k*N+j] ;
      a[i*N+j] = sum ;
    }
  }
}


int main()
{
  float *a, *b, *c;
  float *a_ver, *b_ver, *c_ver;
  int i;
  double elapsed_time;
  a = (float *) malloc(M*N*sizeof(float));
  b = (float *) malloc(M*P*sizeof(float));
  c = (float *) malloc(P*N*sizeof(float));
  a_ver = (float *) malloc(M*N*sizeof(float));
  b_ver = (float *) malloc(M*P*sizeof(float));
  c_ver = (float *) malloc(P*N*sizeof(float));

  for (i = 0; i <  M*N; i++) {
    a[i] = (float) 0.0F;
    a_ver[i] = (float) 0.0F;
  }
  for (i = 0; i <  M*P; i++) {
    b[i] = (float) i;
    b_ver[i] = (float) i;
  }
  for (i = 0; i <  P*N; i++) {
    c[i] = (float) 1.0F;
    c_ver[i] = (float) 1.0F;
  }

  printf("M: %d, N: %d, P: %d\n", M, N, P);

  elapsed_time = my_timer();
  MatrixMultiplication_directive(a,b,c);
  elapsed_time = my_timer() - elapsed_time;
  printf("Directive Elapsed time = %lf sec\n", elapsed_time);
#if VERIFICATION == 1
  elapsed_time = my_timer();
  MatrixMultiplication_verify(a_ver,b_ver,c_ver);
  elapsed_time = my_timer() - elapsed_time;
  printf("Serial Elapsed time = %lf sec\n", elapsed_time);
#endif

#if VERIFICATION == 1
  {
    double dir_sum = 0.0;
    double ver_sum = 0.0;
    double rel_err = 0.0;

    for (i=0; i<M*N; i++){
      ver_sum += a_ver[i]*a_ver[i];
      dir_sum += a[i]*a[i];
    }

    ver_sum = sqrt(ver_sum);
    dir_sum = sqrt(dir_sum);
    if( ver_sum > dir_sum ) {
      rel_err = (ver_sum-dir_sum)/ver_sum;
    } else {
      rel_err = (dir_sum-ver_sum)/ver_sum;
    }

    if(rel_err < 1e-6)
    {
      printf("Verification Successful err = %e\n", rel_err);
    }
    else
    {
      printf("Verification Fail err = %e\n", rel_err);
    }
  }
#endif

  free(a_ver);
  free(b_ver);
  free(c_ver);
  free(a);
  free(b);
  free(c);

  return 0;
} 
