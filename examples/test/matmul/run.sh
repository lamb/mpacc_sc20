#!/bin/bash

RUN_OM4=0
RUN_ACC=1

echo "-------------------------" > results.txt


# OpenMP 4.5 Versions
if [ $RUN_OM4 -eq 1 ]
then
  for value in 1 2 3 4 5 6
  do
    make purge;
    make V_OM3=0 V_OM4=$value V_ACC=0;
    echo "OpenMP 4 Version $value" >> results.txt
    ./matmul >> results.txt
    echo "-----------------"
    echo "-----------------" >> results.txt
  done
fi
echo "-------------------------" >> results.txt
  
# OpenACC Versions
if [ $RUN_ACC -eq 1 ]
then
  for value in 1 2 3 4 5
  do
    make purge;
    make V_OM3=0 V_OM4=0 V_ACC=$value;
    echo "OpenACC Version $value" >> results.txt
    ./matmul >> results.txt
    echo "-----------------"
    echo "-----------------" >> results.txt
  done
fi
