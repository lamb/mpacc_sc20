#include <stdio.h>
#include <stdlib.h>

int my_add(a, b) {
  return a + b;
}

int main() {

  int sum = 0;
  int a[1000], b[1000], c[1000];

  for (int i = 0; i < 1000; ++i) {
    a[i] = i;
    b[i] = 2*i;
  }

#pragma omp target map(to:a[0:1000], b[0:1000]) map(from:c[0:1000])

// Scalar Assignment 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Vectorized
// PGI -fast -mp: Loop unrolled 2 times
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {

#pragma omp simd
    for (int j = 0; j < 100; ++j) {
      c[i] = j; 
    }
  }
#endif

// Array Indexing
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Not Vectorized
// PGI -fast -mp: Loop not vectorized: non-stride-1 array reference
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {

#pragma omp simd
    for (int j = 0; j < 100; ++j) {
      c[j] = a[j] + b[j]; 
    }
  }
#endif
  
// Function Call 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Vectorized
// PGI -fast -mp: my_add inlined, size=2 (inline) file simd.c (4), Loop not vectorized: may not be beneficial
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {

#pragma omp simd
    for (int j = 0; j < 100; ++j) {
      c[i] = my_add(j, j);
    }
  }
#endif

// Reduction 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Vectorized
// PGI -fast -mp: Loop not vectorized: non-stride-1 array reference
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {

#pragma omp simd reduction(+:sum)
    for (int j = 0; j < 100; ++j) {
      sum += 4;
    }
  }
#endif

// Irregular array accesses 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Vectorized
// PGI -fast -mp: Loop unrolled 4 times (Not Vectorized)
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {
    int index = 4;

#pragma omp simd
    for (int j = 0; j < 100; ++j) {
      c[index] = 4;
      index += 2;
    }
  }
#endif

  // Control Flow Divergence
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Not Vectorized
// PGI -fast -mp: (Not Vectorized)
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {
    int index = 4;

#pragma omp simd
    for (int j = 0; j < 100; ++j) {
      if (j % 2 == 0) 
        c[i] = a[i];
      else
        c[i] = b[i];
    }
  }
#endif

// Inner Loops 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Not Vectorized
// PGI -fast -mp: Loop not vectorized: non-stride-1 array reference
#if 1
#pragma omp teams distribute parallel for 
  for (int i = 0; i < 1000; ++i) {
    int index = 4;

#pragma omp simd
    for (int j = 0; j < 100; ++j) {
      for (int k = 0; k < 10; ++k) {
        c[i] = a[i] + b[i];
      }
    }
  }
#endif

  // Outer Loops 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Vectorized
// PGI -fast -mp: Loop not vectorized: data dependency
#if 0
#pragma omp teams distribute simd
  for (int i = 0; i < 1000; ++i) {
    c[i] = a[i] + b[i];
  }
#endif

// Parallel For Loops 
// Clang -fopenmp-targets=x86_64-unknown-linux-gnu: Not Vectorized
// PGI -fast -mp: (outer) Loop not vectorized/parallelized: contains call, (inner) Loop not vectorized: non-stride-1 array reference
#if 1
#pragma omp teams distribute
  for (int i = 0; i < 1000; ++i) {

#pragma omp parallel for simd
    for (int j = 0; j < 100; ++j) {
        c[i] = a[i];
    }
  }
#endif


  printf("Verification Succesful! %d\n", sum);

  return 0;
}

