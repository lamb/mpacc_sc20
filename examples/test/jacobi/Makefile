########################
# Set the program name #
########################
BENCHMARK = jacobi

########################################
# Set the input C source files (CSRCS) #
########################################
CSRCS = *.c

#########################################
# Set macros used for the input program #
#########################################
OMP ?= 1
MODE ?= normal

#########################################################
# Use the following macros to give program-specific     #
# compiler flags and libraries                          #
# - CFLAGS1 and CLIBS1 to compile the input C program   #
# - CFLAGS2 and CLIBS2 to compile the OpenARC-generated #
#   output C++ program                                  # 
#########################################################

CXX=clang
CFLAGS1 = -Wall -O3 -I. -fopenmp -fopenmp-targets=x86_64-unknown-linux-gnu -lm
#CFLAGS1 = -Wall -O3 -I. -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda --cuda-gpu-arch=sm_60 -lm # P100
#CFLAGS1 = -Wall -O3 -I. -lm

#CFLAGS1 = -Wall -O3 -I. -fopenmp -fopenmp-targets=ppc64le-unknown-linux-gnu -lm # Power9
#CFLAGS1 = -Wall -O3 -I. -fopenmp -fopenmp-targets=nvptx64-nvidia-cuda --cuda-gpu-arch=sm_70 -lm # V100

#CXX=pgcc
#CFLAGS1 = -I. -Minfo=all -V19.4 -Mllvm -fast -acc -mp -Mnouniform -ta=multicore
#CFLAGS1 = -I. -Minfo=mp,acc -V19.4 -Mllvm -fast -acc -mp -Mnouniform -ta=tesla:cc60
#CFLAGS1 = -I. -Minfo=mp,acc -V19.4 -Mllvm -fast -acc -mp -Mnouniform -ta=tesla:cc70
#CFLAGS1 = -I. -V19.4 -Mllvm -fast -Mnouniform

#CXX=icc
#CFLAGS1= -Wall -I. -O3 -xAVX -qopenmp -qopenmp-offload=host
#CFLAGS1= -Wall -I. -O3 -xAVX -qno-openmp -parallel

#CXX=xlc
#CFLAGS1= -I. -Wall -O3 -qarch=pwr9 -qsmp=omp -qnooffload #host power9
#CFLAGS1= -I. -Wall -O3 -qsmp=omp -qoffload #device V100
#CFLAGS1= -I. -Wall -O3 -qarch=pwr9 -qnooffload #host power9

################################################
# TARGET is where the output binary is stored. #
################################################
TARGET = .

$(TARGET)/$(BENCHMARK): $(CSRCS)
	$(CXX) $(CFLAGS1) $(CSRCS) -o $(TARGET)/$(BENCHMARK) -DVERSION_OM3=$(V_OM3) -DVERSION_OM4=$(V_OM4) -DVERSION_ACC=$(V_ACC) 

clean:
	rm -f $(TARGET)/$(BENCHMARK)

purge:
	rm -rf bin cetus_output openarcConf.txt $(TARGET)/$(BENCHMARK) *.o
