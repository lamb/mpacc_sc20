#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
  int workers, gangs, size;
  float *A, *B, *C;
  int i;
  int error = 0;

  workers = 10000;
  gangs = 10000;
  size = workers * gangs;

  A = (float*) malloc(size * sizeof(float));
  B = (float*) malloc(size * sizeof(float));
  C = (float*) malloc(size * sizeof(float));

  for (i = 0; i < size; i++) {
    A[i] = (float) i;
    B[i] = (float) i * 100;
  }

  #pragma acc data copyin(A[0:size], B[0:size]) copyout(C[0:size]) 
  { 
    #pragma acc parallel loop gang present(A[0:size], B[0:size], C[0:size])
    for (int i = 0; i < size; i++) {  
      for (int j = 0; j < size; ++j) {
        C[i] = A[i] + B[i];

      }
    }
  }


  for (i = 0; i < size; i++) {
    if (C[i] != A[i] + B[i] ) error++;
  }

  printf("workers:%d, gangs:%d, size:%d, error:%d\n", workers, gangs, size, error);

  return 0;
}
