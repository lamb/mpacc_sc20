#include <stdio.h>
#include <stdlib.h>

int myadd (int a, int b) { return a + b; }
#pragma omp declare target to(myadd)

int main() {

  int a[10000], b[10000], c[10000];

  for (int i = 0; i < 10000; ++i) {
    a[i] = i;
    b[i] = 2*i;
  }

  #pragma omp target teams distribute parallel for map(to:a[0:10000], b[0:10000]) map(from:c[0:10000])
  for (int i = 0; i < 10000; ++i) {
    c[i] = myadd(a[i], b[i]);
  }

  for (int i=0; i < 1000; ++i) {
    if (c[i] != myadd(i + i, i) ) {
      printf("Verification Failed!\n");
      exit(0);
    }
  }

  printf("Verification Succesful!\n");

  return 0;
}

