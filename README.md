# SC20 CCAMP Project 

> Code Repository for the CCAMP SC20 Submission Artifact Description 

---

## Table of Contents

- [Directories](#directories)
- [Environment Setup](#environment-setup)
- [OpenARC Translation](#openarc-translation)
- [LULESH Example](#lulesh-example)
- [SPEC Accel Build](#spec-accel-build)
- [Compilers](#compilers)
- [Authors](#authors)

---

## Directories 

- collect_env - environment details collected by AuthorKit
- examples - contains examples used during development and testing 
- lulesh - lulesh 2.0 OpenACC kernel, including Makefile and argument file
- openarc - OpenARC binary and limited documentation
- python - scripts and data used to generate figures
- spec_configs - configuration files used to specrun applications

---
## Environment Setup

This `setup_env.sh` file must be sourced within a bash environment each time a new terminal session started, so the path environments are properly udated.

```shell
$ source ./setup_env.sh
```

---
## OpenARC Translation

- Example OpenARC translation of LULESH

- Edit opeanrcArgs.txt to choose a desired configuration: 
    - deviceType = 1 for CPU targets
    - deviceType = 0 for GPU targets
    - enableadvancedmapping = 0 (default) to disable CCAMP optimization
    - enableadvancedmapping = 1 to enable CCAMP optimization
    - ompaccInter = 0 (default) disable translation
    - ompaccInter = 2 generate OpenACC directives from OpenMP 4.0 directives
    - ompaccInter = 3 generate OpenMP 3.0 directives from OpenACC directives
    - ompaccInter = 4 generate OpenMP 4.0 directives from OpenACC directives 
    - ompaccInter = 5 generate optimized OpenMP directives from OpenMP directives  
- See ccamp\_sc20/openarc/docs/OpenARC\_parallelismmapping.txt for details

> Run OpenARC

```shell
$ ./O2GBuild.script 
```

- Edit Makefile to target specific compiler and flag-set configuration

---
## LULESH Example

- We have automated the process to generate the results used in the LULESH evaluation figures. The supplied bash script builds, compiles, and executes different versions of the application across selected devices and compilers. Options for which code versions, compilers, and devices to evaluate can be set in the specified bash script (run.sh).

```shell
$ cd lulesh 
$ vim run.sh # specify options
$ run.sh 
```

---
## SPEC Accel Build

- Example SPEC Accel manual execution of 503.postencil using a clang configuration

```shell
$ cd spec_accel/
$ source shrc
$ runspec --action=scrub openmp 
$ runspec --config=clang9.0.0.cfg --noreportable --tune=base --size=ref --iterations=1 503.postencil
```

---
## Compilers 
Below detail how each compiler used in the evaluation was installed.

### Clang 

> Clone the LLVM GitHub repository and checkout version 9.

```shell
$ git clone https://github.com/llvm/llvm-project.git 
$ cd llvm-project
$ git checkout llvmorg-9.0.0-rc6
``` 

> Load an appropriate version of GCC, and build the clang compiler with offloading support (Nvidia P100)

```shell
$ module load gnu/7.5.0
$ mkdir build & cd build;
$ cmake -G "Unix Makefiles" ../llvm \
  -DLLVM_ENABLE_PROJECTS="clang;openmp" \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=$(pwd)/../install \
  -DCMAKE_C_COMPILER=gcc \
  -DCMAKE_CXX_COMPILER=g++ \
  -DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_60 \
  -DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITIES=60
$ make -j32
$ make install -j32
``` 

> (Optional) Rebuild clang and OpenMP using clang

```shell
$ cd build;
$ rm -rf *
$ cmake -G "Unix Makefiles" ../llvm \
 -DLLVM_ENABLE_PROJECTS="clang;openmp" \
 -DCMAKE_BUILD_TYPE=Release \
 -DCMAKE_INSTALL_PREFIX=~/compilers/clang-build-RELEASE_900-rc6 \
 -DCMAKE_C_COMPILER=$(pwd)/../install/bin/clang \
 -DCMAKE_CXX_COMPILER=$(pwd)/../install/bin/clang++ \
 -DCMAKE_CXX_FLAGS=--gcc-toolchain=/auto/software/gcc/x86_64/gcc-7.5.0 \
 -DCMAKE_C_FLAGS=--gcc-toolchain=/auto/software/gcc/x86_64/gcc-7.5.0 \
 -DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_60 \
 -DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITIES=60
$ make -j32
$ make install -j32
``` 

### PGI 

- PGI community edition, previously 19.4 and now 19.10, available here: <a href="https://www.pgroup.com/products/community.htm">https://www.pgroup.com/products/community.htm</a>


```shell
$ mkdir pgi_install
$ mv pgilinux-2019-1910-x86-64.tar.gz pgi_install
$ tar -xvf pgilinux-2019-1910-x86-64.tar.gz 
$ ./install
```

### GCC

- GCC version 10.1 can be installed with offloading support via spack

```shell
$ module load gnu/9.1.0  # or a recent version of GCC
$ git clone https://github.com/spack/spack.git
$ cd spack
$ git checkout releases/latest
$ source share/spack/setup-env.sh
$ spack install curl 
$ spack load curl
$ spack install gcc@10.1.0 +nvptx
$ spack clean # try to reinstall again after clean if errors
$ spack load gcc@10.1.0%gcc@9.1.0
```

### XLC

- Our IBM system already had XLC installed, and thus we have no installation instructions

---

##  Authors

- Jacob Lambert - University of Oregon - jlambert@cs.uoregon.edu
- Seyong Lee - Oak Ridge National Lab - lees2@ornl.gov
- Jeffrey Vetter - Oak Ridge National Lab - vetter@ornl.gov 
- Allen Malony - University of Oregon - malony@cs.uoreogn.edu
